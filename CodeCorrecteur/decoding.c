/**
 * @file decoding.c
 * @author Arash Habibi
 * @author Julien Montavont
 * @version 2.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Functions to decipher the code words
 */

#include <stdio.h>
#include "codingdecoding.h"

void copyDataBitsDecoding(CodeWord_t *cw, char *message, int data_size, int which_code)
{
  int i = 0;
  switch(which_code)
  {
    case 2 :
    {
      for(i=0; i<data_size; i++)
      {
        setNthBitW(&(message[i]), 1, getNthBit(cw[i], 3));
        setNthBitW(&(message[i]), 2, getNthBit(cw[i], 5));
        setNthBitW(&(message[i]), 3, getNthBit(cw[i], 6));
        setNthBitW(&(message[i]), 4, getNthBit(cw[i], 7));
        setNthBitW(&(message[i]), 5, getNthBit(cw[i], 9));
        setNthBitW(&(message[i]), 6, getNthBit(cw[i], 10));
        setNthBitW(&(message[i]), 7, getNthBit(cw[i], 11));
        setNthBitW(&(message[i]), 8, getNthBit(cw[i], 12));
      }
      break;
    }
    default :
    {
      for(i=0; i<data_size; i++)
      {
        setNthBitW(&(message[i]), 1, getNthBit(cw[i], 1));
        setNthBitW(&(message[i]), 2, getNthBit(cw[i], 2));
        setNthBitW(&(message[i]), 3, getNthBit(cw[i], 3));
        setNthBitW(&(message[i]), 4, getNthBit(cw[i], 4));
        setNthBitW(&(message[i]), 5, getNthBit(cw[i], 5));
        setNthBitW(&(message[i]), 6, getNthBit(cw[i], 6));
        setNthBitW(&(message[i]), 7, getNthBit(cw[i], 7));
        setNthBitW(&(message[i]), 8, getNthBit(cw[i], 8));
      }
      break;
    }
  }
}

void errorCorrection(CodeWord_t *cw, int data_size, int which_code)
{
  switch(which_code)
  {
    case 2 :
    {
      int H[4][12]={{1,0,1,0,1,0,1,0,1,0,1,0},
	            {0,1,1,0,0,1,1,0,0,1,1,0},
		    {0,0,0,1,1,1,1,0,0,0,0,1},
		    {0,0,0,0,0,0,0,1,1,1,1,1}};
      
      for(int k=0; k<data_size; k++)
      {
	int err[4]={0};
	for(int j=0; j<4; j++)
	{
	  for(int i=0; i<12; i++)
	  {
	     err[j]+=(getNthBit(cw[k],i+1)*H[j][i]);
	  }
	  err[j]=(err[j]%2);
	}
	int cor=0;
	cor+=err[0];
	cor+=2*err[1];
	cor+=4*err[2];
	cor+=8*err[3];
	if(cor>0&&cor<13)
	{
          setNthBitCW(&(cw[k]), cor, !(getNthBit(cw[k], cor)));
	}
      }
     
      break;
    }
    default : break;
  }

  return;
}

int thereIsError(CodeWord_t *cw, int data_size, int which_code)
{
  int error=0;
  switch(which_code)
  {
    case 0 :
    {
      int somme;
      for(int i=0; i<data_size; i++)
      {
        somme=0;

        for(int j=1; j<9; j++)
          somme+=getNthBit(cw[i],j);
      
        if((somme%2)!=getNthBit(cw[i],9))
          error++;
      }
      break;
    }
    case 1 :
    { 
      int g[9]={0};
      g[0]=1;
      g[4]=1;
      g[5]=1;
      g[6]=1;
      g[8]=1;

      for(int j=0; j<data_size; j++)
      {
	int pos=8;
        int in[16]={0};
	for(int k=1; k<17; k++)
	{
	  in[k-1]=getNthBit(cw[j],k);   
	}
	
	for(int k=0; k<8; k++)
	{
	  if(in[k]==1)
	  {
	    pos=k;
	    break;
	  }
	}
	while(pos<8)
	{
	  for(int k=pos; k<(9+pos); k++)
	  {
	    int l=k-pos;
	    in[k]=((g[l]+in[k])%2);
	  }
	  
	  for(int k=0; k<9; k++)
	  {
	    pos=k;
	    if(in[k]==1)
	    {
	      break;
	    }
	  }
	}
	for(int k=0; k<16; k++)
	{
	  if(in[k]==1)
	  {
	    error++;
	    break;
	  }
	}
      }
      break;
    }
    case 2 :
    {
      int H[4][12]={{1,0,1,0,1,0,1,0,1,0,1,0},
	            {0,1,1,0,0,1,1,0,0,1,1,0},
		    {0,0,0,1,1,1,1,0,0,0,0,1},
		    {0,0,0,0,0,0,0,1,1,1,1,1}};
      
      for(int k=0; k<data_size; k++)
      {
	int err[4]={0};
	for(int j=0; j<4; j++)
	{
	  for(int i=0; i<12; i++)
	  {
	     err[j]+=(getNthBit(cw[k],i+1)*H[j][i]);
	  }
	}
	int cor=0;
	cor+=err[0];
	cor+=2*err[1];
	cor+=4*err[2];
	cor+=8*err[3];
	if(cor!=0)
	 error++;
      }
      break;
    }
    default : break;
  }

  return error;
}


void decoding(char *cw, int cw_size, char *message, int *data_size, int which_code)
{
  *data_size = cw_size / sizeof(CodeWord_t);

  //-- For decoding
  copyDataBitsDecoding((CodeWord_t*)cw, message, *data_size, which_code);
  int error=0;
  if((error=thereIsError((CodeWord_t*)cw, *data_size, which_code)))
    {
      printf("PARITY ERROR: \"%s\" THERE IS %d ERROR(S)\n", message, error);
      for(int i=0; i<*data_size; i++)
      {
        printBits(((CodeWord_t*)cw)[i],message+i);
      }
    }

  errorCorrection((CodeWord_t*)cw, *data_size, which_code);

  //-- For decoding
  copyDataBitsDecoding((CodeWord_t*)cw, message, *data_size, which_code);
  
  for(int i=0; i<*data_size; i++)
  {
    printBits(((CodeWord_t*)cw)[i],message+i);
  }
  return;
}
