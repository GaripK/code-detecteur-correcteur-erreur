/**
 * @file coding.c
 * @author Arash Habibi
 * @author Julien Montavont
 * @version 2.0
 *
 * @section LICENSE
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details at
 * http://www.gnu.org/copyleft/gpl.html
 *
 * @section DESCRIPTION
 *
 * Generate code words from the initial data flow
 */

#include "codingdecoding.h"

void copyDataBitsCoding(char *message, CodeWord_t *cw, int size)
{
  int i = 0;

  for(i=0; i<size; i++)
    {
      setNthBitCW(&(cw[i]), 1, getNthBit(message[i], 1));
      setNthBitCW(&(cw[i]), 2, getNthBit(message[i], 2));
      setNthBitCW(&(cw[i]), 3, getNthBit(message[i], 3));
      setNthBitCW(&(cw[i]), 4, getNthBit(message[i], 4));
      setNthBitCW(&(cw[i]), 5, getNthBit(message[i], 5));
      setNthBitCW(&(cw[i]), 6, getNthBit(message[i], 6));
      setNthBitCW(&(cw[i]), 7, getNthBit(message[i], 7));
      setNthBitCW(&(cw[i]), 8, getNthBit(message[i], 8));
    }

  return;
}

void computeCtrlBits(CodeWord_t *message, int size, int which_code )
{
  switch(which_code)
  {
    case 0 :
    {
      int somme;
      for(int i=0; i<size; i++)
      {
        somme=0;
        for(int j=1; j<9; j++)
          somme+=getNthBit(message[i],j);
      
        setNthBitCW(&(message[i]), 9, (somme%2));
      }
      break;
    }
    case 1 :
    {
      int g[9]={0};
      g[0]=1;
      g[4]=1;
      g[5]=1;
      g[6]=1;
      g[8]=1;

      for(int j=0; j<size; j++)
      {
	int pos=8;
        int i[16]={0};
	for(int k=1; k<9; k++)
	{
	  i[k-1]=getNthBit(message[j],k);
	}
	
	for(int k=0; k<8; k++)
	{
	  if(i[k]==1)
	  {
	    pos=k;
	    break;
	  }
	}
	while(pos<8)
	{
	  for(int k=pos; k<(9+pos); k++)
	  {
	    int l=k-pos;
	    i[k]=((g[l]+i[k])%2);
	  }
	  
	  for(int k=0; k<9; k++)
	  {
	    pos=k;
	    if(i[k]==1)
	    {
	      break;
	    }
	  }
	}
	for(int k=8; k<16; k++)
	{
	  setNthBitCW(&(message[j]), k+1, i[k]);
	}
      }


      break;
    }
    case 2 :
    {
      int G[8][12]={{1,1,1,0,0,0,0,0,0,0,0,0},
		    {1,0,0,1,1,0,0,0,0,0,0,0},
		    {0,1,0,1,0,1,0,0,0,0,0,0},
		    {1,1,0,1,0,0,1,0,0,0,0,0},
		    {1,0,0,0,0,0,0,1,1,0,0,0},
		    {0,1,0,0,0,0,0,1,0,1,0,0},
		    {1,1,0,0,0,0,0,1,0,0,1,0},
		    {0,0,0,1,0,0,0,1,0,0,0,1}};
      for(int k=0; k<size; k++)
      {
	int code[12]={0};
        for(int i=0; i<12; i++)
        {
	  for(int j=0; j<8; j++)
	  {
	    code[i]+=(getNthBit(message[k],j+1)*G[j][i]);
	  }
	  code[i]=(code[i]%2);
        }
        for(int i=0; i<12; i++)
        {
	  setNthBitCW(&(message[k]), i+1, code[i]);
	}
      }
      break;
    }
    default : break;
  }
  return;
}

void coding(char *message, int data_size, char *cw, int *cw_size, int which_code)
{
  *cw_size = data_size * sizeof(CodeWord_t);

  copyDataBitsCoding(message, (CodeWord_t*)cw, data_size);
  computeCtrlBits((CodeWord_t*)cw, *cw_size, which_code);
  for(int i=0; i<data_size; i++)
  {
     printBits(((CodeWord_t*)cw)[i],message+i);
  }
  return;
}
